# Knapsack

`knapsack` is an [`ncurses`][ncurses] style interface for the [BagIt File Packaging Format][bagit].

It allows large scale creation, management, and validation of Bags on your filesystem.

## Documentation

_TK_.

## Development

`knapsack` is written in Rust and uses the [`cursive`][cursive] TUI library.

To build and run a development copy, [install Rust][rust-install] and do:

```sh
git clone https://gitlab.com/no_reply/knapsack.git; cd knapsack

cargo run
```

## License

`knapsack` is published under the [MIT License][license].

[bagit]: https://tools.ietf.org/html/rfc8493
[cursive]: https://docs.rs/cursive
[license]: ./LICENSE
[ncurses]: https://en.wikipedia.org/wiki/Ncurses
[rust-install]: https://www.rust-lang.org/tools/install
